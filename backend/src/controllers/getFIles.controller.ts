import { Request, Response } from "express";
import { AppDataSource } from "../data-source"
import { UploadFiles } from "../models/Upload.model";

export const getFilesController = async (req: Request, res: Response) => {

  const listFiles = await AppDataSource.manager.find(UploadFiles)

  return res.status(200).json(listFiles)
};