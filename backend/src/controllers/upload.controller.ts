import { Request, Response } from "express";
import { AppDataSource } from "../data-source"
import { UploadFiles } from "../models/Upload.model";

export const uploadController = async (req: Request, res: Response) => {

  const { filename, path, mimetype, size } = req.file

  const file = new UploadFiles()
  file.filename = filename
  file.path = path
  file.type = mimetype

  await AppDataSource.manager.save(file)

  return res.status(200).json(file)
};