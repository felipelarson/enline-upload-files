import { Request, Response } from "express"
import { AppDataSource } from "../data-source"
import { User } from "../models/User"

export const userController = async (req: Request, res: Response) => {
  const { firstName, lastName, age } = req.body

  const user = new User()
  user.firstName = firstName
  user.lastName = lastName
  user.age = age

  const users = await AppDataSource.manager.find(User)

  await AppDataSource.manager.save(user)

  res.json(users)
}