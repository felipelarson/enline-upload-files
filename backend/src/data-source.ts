import "reflect-metadata"
import { DataSource } from "typeorm"
import { User } from "./models/User"
import { UploadFiles } from './models/Upload.model'

const { URI_DATABASE } = process.env;

export const AppDataSource = new DataSource({
    type: "mongodb",
    database: "enline",
    url: URI_DATABASE,
    synchronize: true,
    logging: false,
    entities: [User, UploadFiles],
    migrations: [],
    subscribers: [],
    useUnifiedTopology: true
})
