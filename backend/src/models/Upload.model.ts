import { Entity, Column, ObjectIdColumn, ObjectID } from "typeorm"

@Entity()
export class UploadFiles {

  @ObjectIdColumn()
  id: ObjectID

  @Column()
  filename: string

  @Column()
  path: string

  @Column()
  type: string

}