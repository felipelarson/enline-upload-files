import { Router } from "express";
import multer from "multer"
import { getFilesController } from "../controllers/getFIles.controller";
import { uploadController } from "../controllers/upload.controller";
import { upload } from "../middleware/upload.middleware";

export const router = Router();

router.post("/upload", upload.single('file'), uploadController)
router.get("/", getFilesController)