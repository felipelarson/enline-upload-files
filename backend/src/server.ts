import { AppDataSource } from "./data-source"
import app from './app'
import dotenv from "dotenv"

dotenv.config()

const { PORT } = process.env;

app.listen(PORT, async () => {
  try {
    await AppDataSource.initialize();

    console.log(`
        Server up port: http://localhost:${PORT}
      `);
  } catch (error) {
    if (error instanceof Error) {
      console.log(`AppDataSource: ${error.message}`);

      process.exit(1);
    }
  }
});