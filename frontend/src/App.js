import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { Button, Input } from "./components";
import { api } from "./services/api";
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const onSubmit = (data) => {
  const formData = new FormData()

  formData.append("file", data.file[0])

  api
    .post("/upload", formData, {
      headers: {
        'Content-Type': `multipart/form-data`,
      }
    })
    .then((_) => toast("Register success!", {
      position: "top-center",
      autoClose: 3000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    }))
    .catch((err) => console.log(err));
};

export const App = () => {

  const { register, handleSubmit } = useForm();
  const [listFiles, setListFiles] = useState([])

  useEffect(() => {
    api
      .get("/")
      .then((response) => {
        setListFiles([...listFiles, response.data])

      })
      .catch((err) => console.log(err));
    // eslint-disable-next-line
  }, [setListFiles])

  return (
    <>
      <ToastContainer />
      <h1 className="title">Enline - Page to Upload Files</h1>
      <form className="form" onSubmit={handleSubmit(onSubmit)}>
        <Input register={register} />
        <Button />
      </form>
      <ul>
        {
          listFiles.map((file) => file.map(item => <li key={item.id}><a href={item.path} download>{item.filename}</a></li>))
        }
      </ul>
    </>
  );
}
