import { useEffect, useState } from "react"
import { PreviewImg } from "../PreviewImg"


export const Input = ({ register }) => {

  const [selectedFile, setSelectedFile] = useState()
  const [preview, setPreview] = useState()

  useEffect(() => {
    if (!selectedFile) {
      setPreview(undefined)
      return
    }

    const objectUrl = URL.createObjectURL(selectedFile)
    setPreview(objectUrl)

    return () => URL.revokeObjectURL(objectUrl)
  }, [selectedFile])

  const onSelectFile = e => {
    if (!e.target.files || e.target.files.length === 0) {
      setSelectedFile(undefined)
      return
    }

    setSelectedFile(e.target.files[0])
  }

  return (
    <>
      {selectedFile && <PreviewImg data={preview} />}<br />
      <label htmlFor="file-upload" className="custom-file-upload">
        Select File
      </label>
      <input id="file-upload" type="file" name="file" onChange={onSelectFile} {...register('file')} />
    </>
  )
}
