import React from 'react'

export const PreviewImg = ({ data }) => {

  return (
    <img id="preview-img" src={data} alt='' />
  )
}
