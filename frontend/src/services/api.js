import axios from "axios";

export const api = axios.create({
  baseURL: "https://enline-upload-files.herokuapp.com/"
})